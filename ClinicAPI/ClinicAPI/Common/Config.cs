﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ClinicAPI
{
    public class Config
    {
        private string[] _UrlsToRun;

        public string DatabaseConnectionString { get; private set; }
        public string[] UrlsToRun
        {
            get
            {
                return _UrlsToRun;
            }
        }

        public Config()
        {
            ReadConfig();
        }

        private void ReadConfig()
        {
            ConfigurationBuilder objConfigurationBuilder = new ConfigurationBuilder();
            string strConfigPath = Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json");
            objConfigurationBuilder.AddJsonFile(strConfigPath, false);
            IConfigurationRoot root = objConfigurationBuilder.Build();
            DatabaseConnectionString = root.GetSection("ConnectionString").Value;
            _UrlsToRun = root.GetSection("UrlsToUse").Value.Split('~');
        }
    }
}
