﻿namespace ClinicAPI
{
    public class Routes
    {
        public const string Status = "Status";

        #region User Routes

        public const string User = "Users";

        public const string CreateUser = "CreateUser";
        public const string DeleteUser = "DeleteUser";
        public const string UpdateUser = "UpdateUser";
        public const string GetUserDetails = "GetUserDetails";


        public const string Clinic = "Clinic";
        public const string SaveClinic = "SaveClinic";
        public const string GetClinic = "GetClinic";

        #endregion
    }
}
