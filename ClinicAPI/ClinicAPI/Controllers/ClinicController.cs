﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ClinicAPI.Models;

namespace ClinicAPI.Controllers
{
    [Route(Routes.Clinic)]
    [ApiController]
    public class ClinicController : ControllerBase
    {
        private ClinicDataContext _objClinicDataContext;

        public ClinicController(ClinicDataContext pobjClinicalDataContext)
        {
            _objClinicDataContext = pobjClinicalDataContext;
        }

        [HttpPost(Routes.SaveClinic)]
        public string SaveClinicData([FromBody] object pstrJsonData)
        {
            Clinic enClinic = JsonConvert.DeserializeObject<Clinic>(pstrJsonData.ToString());
            enClinic.Adminuserid = enClinic.Adminuserid.HasValue ? enClinic.Adminuserid : _objClinicDataContext.Users.First().UserId;
            if (_objClinicDataContext.Clinic.Where(X => X.Clinicname == enClinic.Clinicname).Any())
            {
                return Messages.ClinicNameAlreadyExists;
            }
            _objClinicDataContext.Clinic.Add(enClinic);

            _objClinicDataContext.SaveChanges();

            return Messages.SUCCESS;
        }

        [HttpPost(Routes.GetClinic)]
        public List<Clinic> GetAllClinics()
        {
            return _objClinicDataContext.Clinic.ToList();
        }
    }
}