﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ClinicAPI.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ClinicAPI.Controllers
{
    [ApiController]
    [Route(Routes.Status)]
    public class StatusController : ControllerBase
    {
        private ClinicDataContext _objClinicDataContext;

        public StatusController(ClinicDataContext pobjClinicalDataContext)
        {
            _objClinicDataContext = pobjClinicalDataContext;
        }

        [HttpGet]
        public IEnumerable<string> Status()
        {
            string strDBStatus = string.Empty;
            try
            {
                int iUserCount = _objClinicDataContext.UserTypeMaster.Count();
                strDBStatus = "Alive";
            }
            catch (Exception ex)
            {
                strDBStatus = ex.ToString();
            }
            return new string[] { "Running : True", $"DBConnection : {strDBStatus}" };
        }

    }
}
