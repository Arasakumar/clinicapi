﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ClinicAPI.Controllers
{
    [ApiController]
    [Route(Routes.User)]
    public class UserController : ControllerBase
    {
        [HttpPost(Routes.CreateUser)]
        public long CreateUser()
        {
            long lUserID = -1;
            return lUserID;
        }

        [HttpPost(Routes.DeleteUser)]
        public bool DeleteUser()
        {
            bool bIsUserDeleted = false;
            return bIsUserDeleted;
        }

        [HttpPost(Routes.UpdateUser)]
        public bool UpdateUser()
        {
            bool bIsUserUpdated = false;
            return bIsUserUpdated;
        }

        [HttpPost(Routes.GetUserDetails)]
        public object GetUserDetails()
        {
            return null;
        }
    }
}
