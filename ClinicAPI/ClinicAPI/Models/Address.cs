﻿using System;
using System.Collections.Generic;

namespace ClinicAPI.Models
{
    public partial class Address
    {
        public long AddressId { get; set; }
        public string Address1 { get; set; }
        public string District { get; set; }
        public string State { get; set; }
        public string Pincode { get; set; }
        public string Landmark { get; set; }
    }
}
