﻿using System;
using System.Collections.Generic;

namespace ClinicAPI.Models
{
    public partial class Clinic
    {
        public int? Clinicid { get; set; }
        public string Clinicname { get; set; }
        public string Clinicspeciality { get; set; }
        public string Clinicregid { get; set; }
        public string Number1 { get; set; }
        public string Number2 { get; set; }
        public string Number3 { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Pincode { get; set; }
        public string About { get; set; }
        public long? Adminuserid { get; set; }

        public virtual Users Adminuser { get; set; }
    }
}
