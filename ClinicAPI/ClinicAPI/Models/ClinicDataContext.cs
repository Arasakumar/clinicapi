﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ClinicAPI.Models
{
    public partial class ClinicDataContext : DbContext
    {
        public ClinicDataContext()
        {
        }

        public ClinicDataContext(DbContextOptions<ClinicDataContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Address> Address { get; set; }
        public virtual DbSet<Clinic> Clinic { get; set; }
        public virtual DbSet<UserTypeMaster> UserTypeMaster { get; set; }
        public virtual DbSet<Users> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseNpgsql("Host=localhost;Database=ClinicData;Username=postgres;Password=1234");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Address>(entity =>
            {
                entity.Property(e => e.AddressId)
                    .HasColumnName("AddressID")
                    .UseIdentityAlwaysColumn();

                entity.Property(e => e.Address1)
                    .IsRequired()
                    .HasColumnName("Address")
                    .HasMaxLength(500);

                entity.Property(e => e.District)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Landmark).HasMaxLength(100);

                entity.Property(e => e.Pincode)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.State)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<Clinic>(entity =>
            {
                entity.ToTable("clinic");

                entity.Property(e => e.Clinicid)
                    .HasColumnName("clinicid")
                    .UseIdentityAlwaysColumn();

                entity.Property(e => e.About)
                    .HasColumnName("about")
                    .HasMaxLength(500);

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasColumnName("address")
                    .HasMaxLength(500);

                entity.Property(e => e.Adminuserid).HasColumnName("adminuserid");

                entity.Property(e => e.City)
                    .HasColumnName("city")
                    .HasMaxLength(100);

                entity.Property(e => e.Clinicname)
                    .IsRequired()
                    .HasColumnName("clinicname")
                    .HasMaxLength(200);

                entity.Property(e => e.Clinicregid)
                    .HasColumnName("clinicregid")
                    .HasMaxLength(20);

                entity.Property(e => e.Clinicspeciality)
                    .HasColumnName("clinicspeciality")
                    .HasMaxLength(100);

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasMaxLength(100);

                entity.Property(e => e.Number1)
                    .IsRequired()
                    .HasColumnName("number1")
                    .HasMaxLength(20);

                entity.Property(e => e.Number2)
                    .HasColumnName("number2")
                    .HasMaxLength(20);

                entity.Property(e => e.Number3)
                    .HasColumnName("number3")
                    .HasMaxLength(20);

                entity.Property(e => e.Pincode)
                    .HasColumnName("pincode")
                    .HasMaxLength(20);

                entity.Property(e => e.State)
                    .HasColumnName("state")
                    .HasMaxLength(100);

                entity.HasOne(d => d.Adminuser)
                    .WithMany(p => p.Clinic)
                    .HasForeignKey(d => d.Adminuserid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_adminuserid");
            });

            modelBuilder.Entity<UserTypeMaster>(entity =>
            {
                entity.HasComment("User Type master data");

                entity.Property(e => e.UserTypeMasterId)
                    .HasColumnName("UserTypeMasterID")
                    .UseIdentityAlwaysColumn();

                entity.Property(e => e.UserTypeMasterName).HasMaxLength(50);
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.HasKey(e => e.UserId)
                    .HasName("pk_User_UserID");

                entity.HasComment("Ontains the Minimum details about the user");

                entity.Property(e => e.UserId)
                    .HasColumnName("UserID")
                    .UseIdentityAlwaysColumn();

                entity.Property(e => e.IsActive)
                    .IsRequired()
                    .HasDefaultValueSql("true");

                entity.Property(e => e.LastLogInOn).HasColumnType("date");

                entity.Property(e => e.MailId)
                    .IsRequired()
                    .HasColumnName("mailID")
                    .HasMaxLength(100);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.PhoneNumber)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.UserTypeMasterId).HasColumnName("UserTypeMasterID");

                entity.HasOne(d => d.UserTypeMaster)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.UserTypeMasterId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_User_UsertypeMaster");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
