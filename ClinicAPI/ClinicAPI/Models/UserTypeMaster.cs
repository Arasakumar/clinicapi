﻿using System;
using System.Collections.Generic;

namespace ClinicAPI.Models
{
    public partial class UserTypeMaster
    {
        public UserTypeMaster()
        {
            Users = new HashSet<Users>();
        }

        public int UserTypeMasterId { get; set; }
        public string UserTypeMasterName { get; set; }

        public virtual ICollection<Users> Users { get; set; }
    }
}
