﻿using System;
using System.Collections.Generic;

namespace ClinicAPI.Models
{
    public partial class Users
    {
        public Users()
        {
            Clinic = new HashSet<Clinic>();
        }

        public long UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int UserTypeMasterId { get; set; }
        public string MailId { get; set; }
        public string PhoneNumber { get; set; }
        public int FailedLoginCount { get; set; }
        public DateTime? LastLogInOn { get; set; }
        public bool IsBlocked { get; set; }
        public bool? IsActive { get; set; }

        public virtual UserTypeMaster UserTypeMaster { get; set; }
        public virtual ICollection<Clinic> Clinic { get; set; }
    }
}
